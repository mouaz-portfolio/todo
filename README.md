# Task Manager ~ Mouaz MOHAMED
Task Manager (ou Todo) est une application Web développé avec le Framework PHP CodeIgniter4 permettant de gérer les tâches (ajouter/modifier/supprimer/terminer/réordonner) liées à une base de données MariaDB développée en SQL. On peut également voir les journalisations (logs) de chaque action effectué.

---

## Architecture MVC (Model - View - Controller)
CodeIgniter4 est un framework PHP suivant l'architecture MVC :
- Model (Modèle) : Ce composant représente les données de l'application ainsi que la logique métier. Le modèle gère la manipulation et le traitement des données, telles que la récupération depuis une base de données, leur validation et leur mise à jour.
- View (Vue) : Ce composant est responsable de l'affichage des données au sein de l'interface utilisateur. Elle présente les informations provenant du modèle sous une forme compréhensible pour l'utilisateur final. Dans le contexte web, la vue est généralement une page HTML qui affiche les données dynamiquement.
- Controller (Contrôleur) : Ce composant agit comme un intermédiaire entre le modèle et la vue. Il reçoit les entrées de l'utilisateur, interagit avec le modèle en conséquence et met à jour la vue en conséquence. En d'autres termes, le contrôleur traite les demandes de l'utilisateur, manipule les données en conséquence et coordonne finalement la réponse de l'application.<br><br>
Représentation des interactions entre le modèle, la vue et le contrôleur :<br>
![mvc](Annexes/mvc.png)

---

## Diagramme des cas d'utilisation (User Case Diagram) de mon application Web
```plantuml
left to right direction
:Utilisateur: as Utilisateur
package <uc>Task_Manager{
    Utilisateur --> (Consulter les tâches)
    Utilisateur --> (Ajouter une tâche)
    Utilisateur --> (Modifier une tâche)
    Utilisateur --> (Supprimer une tâche)
    Utilisateur --> (Réordonner les tâches)
    Utilisateur --> (Valider une tâche)
}
```

## Accueil de mon application
Voici l'accueil de mon application Web :
![accueil](Annexes/accueil.png)

## Ajouter une tâche
Nous pouvons ajouter une tâche en précisant le nom de la tâche, ainsi que l'ordre de cette tâche :<br>
![ajout1](Annexes/ajout1.png)<br>
![ajout2](Annexes/ajout2.png)

---

## Modifier une tâche
Nous pouvons modifier une tâche en modifiant le nom de la tâche, ainsi que l'ordre de cette tâche :<br>
![mod1](Annexes/mod1.png)<br>
![mod2](Annexes/mod2.png)<br>
![mod3](Annexes/mod3.png)<br>

---

## Supprimer une tâche
Nous pouvons supprimer une tâche :<br>
![supp1](Annexes/supp1.png)<br>
![supp2](Annexes/supp2.png)<br>

---

## Terminer une tâche
Nous pouvons terminer une tâche :<br>
![term](Annexes/term.png)<br>

---

## Réordonner les tâches
Nous pouvons réordonner les tâches en précisant l'ordre de chaque tâche :<br>
![reord1](Annexes/reord1.png)<br>
![reord2](Annexes/reord2.png)<br>
![reord3](Annexes/reord3.png)<br>

---

## Journalisation (logs) de chaque action effectuée par l'utilisateur dans l'application
Côté Back-End, nous pouvons voir la journalisation (logs) de chaque action effectuée par l'utilisateur via des fichiers `log` triés par date. Ces fichiers comprennent l'horodatage (date et heure), l'adresse IP de l'utilisateur et l'action effectué par l'utilisateur :<br><br>
![logs](Annexes/logs_journalisation.png)